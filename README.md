# Cloud computing tutorial
#### Topic: Docker multi-cluster configuration

## Made by students of data engineering class:
 - Đỗ Minh Đức - 19133021
 - Nguyễn Văn Quốc Trọng - 19133060
 - Nguyễn Văn Quốc Vĩ - 18133063
## Technology used:
- CI/CD
- Docker
- minIO
- Kong API gateway
- EC2

### Step 1: Create instance from amazon EC2
| Instance | Port open |
| ------ | ------ |
| UI | 5000 |
| backend1 | 8080 |
| backend2 | 8080 |
| minIO | 9000, 9001 |
| runner | no |
| mysql | 3306 |

### Step 1: Setup enviroment for instance
#### * UI
- install docker
```sh
sudo apt install
sudo apt-get install docker.io
sudo chmod 766 /var/run/docker.sock
```
#### * backend 1 + 2
##### - install java and maven
- install java
```sh
sudo apt install
sudo apt install default-jdk
```
- install maven
```sh
sudo apt install maven
sudo chmod +x /etc/profile.d/maven.sh
```
#####  - install docker
- Download the binary for your system
```sh
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
```
- Give it permission to execute
```sh
sudo chmod +x /usr/local/bin/gitlab-runner
```
- Create a GitLab Runner user
```sh
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```
- Install and run as a service
```sh
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

**Have a nice day, Hell Yeah!**